// CORNET Allan - DIGITEO - 2011

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros("dynamic_link_windows", macros_path);
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

