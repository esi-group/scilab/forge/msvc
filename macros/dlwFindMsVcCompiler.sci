// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010-2011  - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//=============================================================================
function MSCompiler = dlwFindMsVcCompiler()
  MSCompiler = 'unknown'; // unknown

  // We use always last version of MS compiler

  if dlwIsVc10Pro() & dlwIsVc10Express() then
    MSCompiler = 'msvc100express';     // Microsoft Visual 2010 Express with SDK extension
    return;
  end

  if dlwIsVc10Pro() then
    MSCompiler = 'msvc100pro';       // Microsoft Visual 2010 Professional (or more)
    return;
  end

  if dlwIsVc10Express() then
    MSCompiler = 'msvc100express';     // Microsoft Visual 2010 Express
    return;
  end
endfunction
//=============================================================================
